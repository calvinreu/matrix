#pragma once
#include <memory>
#include <new>
#include <iostream>
#include <string.h>

//Container with Data types
template<typename T, typename allocator = std::allocator<T>, typename PtrAllocator = std::allocator<T*>>
class matrix{
private:

	T** m_data;
	size_t m_height, m_width;
	allocator memory;
	PtrAllocator PtrMemory;

protected:

	class iterator
	{
	private:
		T** m_data;
		size_t* m_size;
		iterator(T* dataPtr, size_t* width) : m_data(dataPtr), m_size(width) {}
	public:
		bool operator< (const matrix<T, allocator, PtrAllocator>::iterator &other) const { return m_data  < other.m_data; }
		bool operator> (const matrix<T, allocator, PtrAllocator>::iterator &other) const { return m_data  > other.m_data; }
		bool operator<=(const matrix<T, allocator, PtrAllocator>::iterator &other) const { return m_data <= other.m_data; }
		bool operator>=(const matrix<T, allocator, PtrAllocator>::iterator &other) const { return m_data >= other.m_data; }
		bool operator==(const matrix<T, allocator, PtrAllocator>::iterator &other) const { return m_data == other.m_data; }
		bool operator!=(const matrix<T, allocator, PtrAllocator>::iterator &other) const { return m_data != other.m_data; }

			  T& operator* ()	    { return *m_data; }
		const T& operator* () const { return *m_data; }
			  T* operator->()       { return m_data; }
		const T* operator->() const { return m_data; }
			  T* operator& ()       { return  m_data; }
		const T* operator& () const { return  m_data; }

		void operator++(int){ m_data++; }
		void operator--(int){ m_data--; }

		void operator+=(const size_t &ammount){ m_data += ammount; }
		void operator-=(const size_t &ammount){ m_data -= ammount; }

		void operator=(const matrix<T, allocator, PtrAllocator>::iterator &other){ m_data = other.m_data; }

		matrix<T, allocator, PtrAllocator>::iterator operator+(const size_t &ammount){ return matrix<T, allocator, PtrAllocator>::iterator(m_data + ammount); }
		matrix<T, allocator, PtrAllocator>::iterator operator-(const size_t &ammount){ return matrix<T, allocator, PtrAllocator>::iterator(m_data - ammount); }
		size_t operator+(const matrix<T, allocator, PtrAllocator>::iterator &other){ return m_data + other.m_data; }
		size_t operator-(const matrix<T, allocator, PtrAllocator>::iterator &other){ return m_data - other.m_data; }

		T* begin (){ return *(m_data            ); }
		T* end   (){ return *(m_data + m_size   ); }
		T* rbegin(){ return *(m_data + m_size -1); }
		T* rend  (){ return *(m_data - 1        ); }

	};


public:

	matrix();
	matrix(const size_t &height, const size_t &width);
	~matrix();
	matrix(     matrix<T, allocator, PtrAllocator> &&other);
	matrix(const matrix<T, allocator, PtrAllocator> &other);

	T& operator()(const size_t &row, const size_t &index){ return m_data[row][index]; }
	const T& operator()(const size_t &row, const size_t &index) const { return m_data[row][index]; }
	matrix<T, allocator, PtrAllocator>& operator+(const matrix<T, allocator, PtrAllocator> &other) const;
	matrix<T, allocator, PtrAllocator>& operator-(const matrix<T, allocator, PtrAllocator> &other) const;
	matrix<T, allocator, PtrAllocator>& operator*(const matrix<T, allocator, PtrAllocator> &other) const;
	matrix<T, allocator, PtrAllocator>& operator/(const matrix<T, allocator, PtrAllocator> &other) const;
	void operator-=(const matrix<T, allocator, PtrAllocator> &other){ *this = (*this) - other; }
	void operator+=(const matrix<T, allocator, PtrAllocator> &other){ *this = (*this) + other; }
	void operator*=(const matrix<T, allocator, PtrAllocator> &other){ *this = (*this) * other; }
	void operator/=(const matrix<T, allocator, PtrAllocator> &other){ *this = (*this) / other; }
	void operator=(const matrix<T, allocator, PtrAllocator>  &other);
	void operator=(	     matrix<T, allocator, PtrAllocator> &&other);

	const size_t& width() const { return m_width; }
	const size_t& height() const { return m_height; }
	T** get_ValPtr(){ return m_data; }
	const T** get_ValPtr() const { return m_data; }
	void resize(const size_t rowC, const size_t row_width);

	matrix<T, allocator, PtrAllocator>::iterator begin (){ return matrix<T, allocator, PtrAllocator>::iterator(m_data              ); }
	matrix<T, allocator, PtrAllocator>::iterator end   (){ return matrix<T, allocator, PtrAllocator>::iterator(m_data + m_height   ); }
	matrix<T, allocator, PtrAllocator>::iterator rbegin(){ return matrix<T, allocator, PtrAllocator>::iterator(m_data + m_height -1); }
	matrix<T, allocator, PtrAllocator>::iterator rend  (){ return matrix<T, allocator, PtrAllocator>::iterator(m_data - 1          ); }

};