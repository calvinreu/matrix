#include <cstring>
#include <new>
#include <memory>
#include <iostream>
#include "matrix.hpp"

template<typename T, typename allocator, typename PtrAllocator>
matrix<T, allocator, PtrAllocator>::matrix()
{
    m_height = 0;
    m_width = 0;
    m_data   = nullptr;
}

template<typename T, typename allocator, typename PtrAllocator>
matrix<T, allocator, PtrAllocator>::matrix(const size_t &height, const size_t &width)
: m_height(height), m_width(width)
{
    try
    {
        m_data = PtrMemory.allocate(m_height);

        for (size_t i = 0; i < m_height; i++)
            m_data[i] = memory.allocate(m_width);        
    }
    catch(std::bad_alloc &e)
    {
        throw e;
    }

}

template<typename T, typename allocator, typename PtrAllocator>
matrix<T, allocator, PtrAllocator>::~matrix()
{
    for (size_t i = 0; i < m_height; i++)
        memory.deallocate(m_data[i], m_width); 

    PtrMemory.deallocate(m_data, m_height);
}

template<typename T, typename allocator, typename PtrAllocator>
matrix<T, allocator, PtrAllocator>::matrix(const matrix<T, allocator, PtrAllocator> &other)
{
    m_height = other.m_height;
    m_width  = other.m_width;
    m_data = PtrMemory.allocate(m_height);

    for (size_t i = 0; i < height; i++){
        m_data[i] = memory.allocate(m_width);
        memcpy(m_data[i] ,other.m_data[i], m_width*sizeof(T));  
    }
}

template<typename T, typename allocator, typename PtrAllocator>
matrix<T, allocator, PtrAllocator>::matrix(matrix<T, allocator, PtrAllocator> &&other)
{
    m_data = other.m_data;
    m_height = other.m_height;
    m_width = other.m_width;
    other.m_height = 0;
}

template<typename T, typename allocator, typename PtrAllocator>
void matrix<T, allocator, PtrAllocator>::operator=(const matrix<T, allocator, PtrAllocator> &other)
{
    if(other.m_width > m_width)
    {
        for (auto i = begin(); i < end(); i++)
            memory.deallocate(*i, m_width);
        
        if(m_height > other.m_height)
        {
            PtrMemory.deallocate(m_data + other.m_height, m_height - other.m_height);
        }
        else
        {
            PtrMemory.deallocate(m_data, m_height);
            m_data = PtrMemory.allocate(other.m_height);
        }  
    }
    else
    {
        size_t temp = m_width - other.m_width;

        if (other.m_height > m_height)
        {
            for (size_t i = 0; i < m_height; i++)
                memory.deallocate(m_data[i] + other.m_width, temp);

            T** tempdata = PtrMemory.allocate(other.m_height);

            memcpy(tempdata, m_data, m_height * sizeof(T*));

            for (size_t i = m_height; i < other.m_height; i++)
                m_data[i] = memory.allocate(other.m_width);
        }
        else
        {
            for (size_t i = other.m_height; i < m_height; i++)
                memory.deallocate(m_data[i], m_width);

            PtrMemory.deallocate(m_data + other.m_height, m_height - other.m_height);
            
            for (size_t i = 0; i < other.m_height; i++)
                memory.deallocate(m_data[i] + other.m_width, temp);
        }
    }

    m_height = other.m_height;
    m_width  = other.m_width ; 

    for (size_t i = 0; i < m_height; i++)
    {
        m_data[i] = memory.allocate(m_width);
        memcpy(m_data[i] ,other.m_data[i], m_width*sizeof(T));            
    }
}

template<typename T, typename allocator, typename PtrAllocator>
void matrix<T, allocator, PtrAllocator>::operator=(matrix<T, allocator, PtrAllocator> &&other)
{
    for (size_t i = 0; i < m_height; i++)
        memory.deallocate(m_data[i], m_width); 

    PtrMemory.deallocate(m_data, m_height);

    m_data = other.m_data;
    m_height = other.m_height;
    m_width = other.m_width;
    other.m_height = 0;
}

template<typename T, typename allocator, typename PtrAllocator>
matrix<T, allocator, PtrAllocator>& matrix<T, allocator, PtrAllocator>::operator+(const matrix<T, allocator, PtrAllocator> &other) const
{
    //if(other.m_height != m_height || other.m_width != m_width)
        //TODO error handling

    matrix<T, allocator, PtrAllocator> retVal(m_height, m_width);

    for (size_t i = 0; i < m_height; i++)
        for (size_t i1 = 0; i1 < m_width; i1++)
            retVal(i, i1) = m_data[i][i1] + other(i, i1);
        
    return retVal;
    
}

template<typename T, typename allocator, typename PtrAllocator>
matrix<T, allocator, PtrAllocator>& matrix<T, allocator, PtrAllocator>::operator-(const matrix<T, allocator, PtrAllocator> &other) const
{
    //if(other.m_height != m_height || other.m_width != m_width)
        //TODO error handling

    matrix<T, allocator, PtrAllocator> retVal(m_height, m_width);

    for (size_t i = 0; i < m_height; i++)
        for (size_t i1 = 0; i1 < m_width; i1++)
            retVal(i, i1) = m_data[i][i1] - other(i, i1);
        
    return retVal;
    
}

template<typename T, typename allocator, typename PtrAllocator>
matrix<T, allocator, PtrAllocator>& matrix<T, allocator, PtrAllocator>::operator*(const matrix<T, allocator, PtrAllocator> &other) const
{
    //if(other.m_height != m_height || other.m_width != m_width)
        //TODO error handling

    matrix<T, allocator, PtrAllocator> retVal(m_height, m_width);

    for (size_t i = 0; i < m_height; i++)
        for (size_t i1 = 0; i1 < m_width; i1++)
            retVal(i, i1) = m_data[i][i1] * other(i, i1);
        
    return retVal;
    
}

template<typename T, typename allocator, typename PtrAllocator>
matrix<T, allocator, PtrAllocator>& matrix<T, allocator, PtrAllocator>::operator/(const matrix<T, allocator, PtrAllocator> &other) const
{
    //if(other.m_height != m_height || other.m_width != m_width)
        //TODO error handling

    matrix<T, allocator, PtrAllocator> retVal(m_height, m_width);

    for (size_t i = 0; i < m_height; i++)
        for (size_t i1 = 0; i1 < m_width; i1++)
            retVal(i, i1) = m_data[i][i1] / other(i, i1);
        
    return retVal;
    
}

template<typename T, typename allocator, typename PtrAllocator>
void matrix<T, allocator, PtrAllocator>::resize(const size_t rowC, const size_t row_width)
{
    if(row_width > m_width)
    {
        for (auto i = begin(); i < end(); i++)
            memory.deallocate(*i, m_width);
        
        if(m_height > rowC)
        {
            PtrMemory.deallocate(m_data + rowC, m_height - rowC);
        }
        else
        {
            PtrMemory.deallocate(m_data, m_height);
            m_data = PtrMemory.allocate(rowC);
        }  
    }
    else
    {
        size_t temp = m_width - row_width;

        if (rowC > m_height)
        {
            for (size_t i = 0; i < m_height; i++)
                memory.deallocate(m_data[i] + row_width, temp);

            T** tempdata = PtrMemory.allocate(rowC);

            memcpy(tempdata, m_data, m_height * sizeof(T*));

            for (size_t i = m_height; i < rowC; i++)
                m_data[i] = memory.allocate(row_width);
        }
        else
        {
            for (size_t i = rowC; i < m_height; i++)
                memory.deallocate(m_data[i], m_width);

            PtrMemory.deallocate(m_data + rowC, m_height - rowC);
            
            for (size_t i = 0; i < rowC; i++)
                memory.deallocate(m_data[i] + row_width, temp);
        }
    }

    m_height = rowC;
    m_width  = row_width ; 
}