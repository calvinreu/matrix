CC = g++ -std=c++17 -c -g -fpic 

default:
	make matrix
	make clean

matrix: matrix.cpp
	$(CC) matrix.cpp -o matrix.o
	gcc -shared matrix.o -o libmatrix.so

clean:
	rm -f matrix.o

install: libmatrix.so matrix.hpp
	mv libmatrix.so /usr/lib
	chmod 0755 /usr/lib/libmatrix.so
	cp matrix.hpp /usr/include/c++/9
	chmod 0755 /usr/include/c++/9 matrix.hpp
